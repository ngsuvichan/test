<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
    <script src="bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="bower_components/angular/angular.min.js" charset="utf-8"></script>
    <div ng-repeat="item in dataList">
        <input type="text" name="name" value="{{$index}}" >
        <button type="button" name="button" ng-click="clickPlus()">+</button>
        <button type="button" name="button" ng-click="clickDelete($index)">-</button>
    </div>



    <script type="text/javascript">
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope) {
            $scope.dataList = [
                {'value':'test'}
            ];
            $scope.clickPlus = function(){
                $scope.dataList.push({'value':'test'})
            }
            $scope.clickDelete = function(index){
                $scope.dataList.splice(index,1);
            }
        });
    </script>
</body>
</html>
