<?php
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/",isset($_SERVER['PATH_INFO'])?$_SERVER['PATH_INFO']:"");
$input = json_decode(file_get_contents('php://input'),true);
array_shift($request);
$key = $request?$request[0]:null;
$response = array();
$item = array(
  "input"=>$input,
  "method"=>$method,
  "path"=>isset($_SERVER['PATH_INFO'])?$_SERVER['PATH_INFO']:"",
  "request"=>$request,
  "key"=>$key
);
for ($i=0; $i < 10; $i++) {
  array_push($response,$item);
}
header('Content-Type: application/json');
echo json_encode($response);
?>
