function wordAccuracy(str1, str2) {

  var len = str1.length,
      distance = levenshtein(str1, str2),
      words1 = str1.split(' '),
      words2 = str2.split(' ');

  return {

    accuracy: 100 - (0|(distance * 100) / len) +'%',

    fail: words1.filter(function(word, idx){
      return word != words2[idx];
    }).length
  }
}

// Example:

var str1 = 'Lorem ipsum dolor sit amet consectetur adipiscing elit';
var str2 = 'Lorme ipsmu dolor sit maet cnsectetur adipiscing elot';

console.log(wordAccuracy(str1, str2));
